package org.droidtr.keyboard;

import android.app.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;

public class setvibrate extends Activity  
{
	boolean pause = false;
	@Override  
	protected void onCreate(Bundle savedInstanceState)  
	{  
		final SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		final SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
		super.onCreate(savedInstanceState);  
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setvibrate);    
		final TextView t = (TextView) findViewById(R.id.t);
		final SeekBar v = (SeekBar) findViewById(R.id.vibrate);
		v.setProgress(read.getInt("vibtime",25));
		v.setMax(100);
		update();
		final  Handler h = new Handler(); 
		final Runnable ru = new Runnable(){
			public void run(){
				if(!pause){
					edit.putInt("vibtime",v.getProgress());
					edit.commit();
					t.setText(v.getProgress()+" ms");
					Boolean vibenable = read.getBoolean("vibenable",true);
					if(vibenable){
					Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
					vib.vibrate(v.getProgress());
					}
					h.postDelayed(this,100);
				}
			}
		};
		h.post(ru);
	}
	
	public void def (View w){
		SeekBar v = (SeekBar) findViewById(R.id.vibrate);
		v.setProgress(25);
	}
	public void okey(View w){
		pause = true;
	    finish();
	}

	
	@Override
	protected void onPause()
	{
		pause = true;
		super.onPause();
		// TODO: Implement this method
		//System.exit(0);
	}

	@Override
	protected void onDestroy()
	{
		// TODO: Implement this method
		onPause();
	}

	@Override
	protected void onStop()
	{
		// TODO: Implement this method
		onPause();
	}
	
	protected void update(){
		TextView t = (TextView) findViewById(R.id.t);
		SeekBar v = (SeekBar) findViewById(R.id.vibrate);
		SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
		Boolean vibenable = read.getBoolean("vibenable",true);
		t.setEnabled(vibenable);
		v.setEnabled(vibenable);
	}
	public void onoff(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
		Boolean vibenable = !read.getBoolean("vibenable",true);
		edit.putBoolean("vibenable",vibenable);
		edit.commit();
		update();
	}
}
