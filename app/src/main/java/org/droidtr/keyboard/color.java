package org.droidtr.keyboard;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import java.io.*;

public class color extends Activity  
{
	String feature="";
	SeekBar a,r,g,b;
	int size=150;
	LinearLayout imagelayout;
	SharedPreferences read;
	  @Override  
	  protected void onCreate(Bundle savedInstanceState)  
	  { 
		final SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		read = getSharedPreferences("key",MODE_PRIVATE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);    
		setContentView(R.layout.color);
		imagelayout = (LinearLayout) findViewById(R.id.imagelayout);
		final LinearLayout i = (LinearLayout) findViewById(R.id.i); 
		  i.setBackgroundColor(read.getInt("color",read.getInt("color",Color.parseColor("#1a252f"))));
		Drawable d = Drawable.createFromPath(Uri.fromFile(new File(getFilesDir()+"/image.png")).getPath());
		imagelayout.setBackgroundDrawable(d);
		a = (SeekBar) findViewById(R.id.a);
		r = (SeekBar) findViewById(R.id.r);
		g = (SeekBar) findViewById(R.id.g);
		b = (SeekBar) findViewById(R.id.b);
		a.setMax(255);
		r.setMax(255);
		g.setMax(255);
		b.setMax(255);
		if(Build.VERSION.SDK_INT <16)
		{
			Button secodaryColor = (Button) findViewById(R.id.secondaryColor);
			secodaryColor.setVisibility(View.GONE);
		}
		a.setVisibility(View.GONE);
		r.setVisibility(View.GONE);  
		g.setVisibility(View.GONE);
		b.setVisibility(View.GONE);
	    final  Handler h = new Handler(); 
		  final Runnable ru = new Runnable(){
			public void run(){
					
				  if(feature.equals("textSizeModifier") || feature.equals("radius") || feature.equals("strokeWidth")){
						  edit.putInt(feature,r.getProgress());
						  setTitle(r.getProgress()+"");
					}else{
						setTitle("#"+Integer.toHexString(a.getProgress())+Integer.toHexString(r.getProgress())+Integer.toHexString(g.getProgress())+Integer.toHexString(b.getProgress()));
						edit.putInt(feature,Color.argb(a.getProgress(),r.getProgress(),g.getProgress(),b.getProgress()));
					  }
					if(!feature.equals("")){
				 	edit.putInt(feature+"_a",a.getProgress());
					edit.putInt(feature+"_r",r.getProgress());  
					edit.putInt(feature+"_g",g.getProgress());
					edit.putInt(feature+"_b",b.getProgress());
					edit.commit();  
					}
					updateBg();
				h.postDelayed(this,50);
			}
		};
		h.post(ru);		
	  }

	
	@Override
	protected void onPause()
	{
		super.onPause();
		// TODO: Implement this method
		//System.exit(0);
	}

	@Override
	protected void onDestroy()
	{
		// TODO: Implement this method
		onPause();
	}

	@Override
	protected void onStop()
	{
		// TODO: Implement this method
		onPause();
	}

	public void changeFeature(View w)
	{
		// TODO: Implement this method
		String tag = w.getTag().toString()+":::::";
		feature=w.getTag().toString().split(":")[0];
		int defa = Integer.parseInt(tag.split(":")[1]);
		int defr = Integer.parseInt(tag.split(":")[2]);
		int defg = Integer.parseInt(tag.split(":")[3]);
		int defb = Integer.parseInt(tag.split(":")[4]);
		if(feature.equals("radius") || feature.equals("textSizeModifier") || feature.equals("strokeWidth")){
			a.setVisibility(View.GONE);
			g.setVisibility(View.GONE);
			b.setVisibility(View.GONE);
			r.setVisibility(View.VISIBLE);
			r.setProgress(read.getInt(feature,defa));
			r.setMax(20);
		} else{
			r.setMax(255);
			a.setProgress(read.getInt(feature+"_a",defa));
			r.setProgress(read.getInt(feature+"_r",defr));
			g.setProgress(read.getInt(feature+"_g",defg));
			b.setProgress(read.getInt(feature+"_b",defb));
			a.setVisibility(View.VISIBLE);
			r.setVisibility(View.VISIBLE);
			g.setVisibility(View.VISIBLE);
			b.setVisibility(View.VISIBLE);
		}
	}
	public void updateBg(){
		final LinearLayout i = (LinearLayout) findViewById(R.id.i);
		final TextView buttontext = (TextView) findViewById(R.id.buttontext);
		final TextView button = (TextView) findViewById(R.id.button);
		int[] color= {read.getInt("primaryColor",Color.TRANSPARENT),read.getInt("secondaryColor",Color.TRANSPARENT)};		
		GradientDrawable gd = new GradientDrawable();
		if(Build.VERSION.SDK_INT >= 16){
			gd.setColors(color);
			gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
		} else{
			gd.setColor(color[0]);
		} 
		gd.setCornerRadius(Float.parseFloat(read.getInt("radius",10)*10+"f"));
		gd.setStroke(read.getInt("strokeWidth",5)*10,read.getInt("strokeColor",Color.TRANSPARENT));
		buttontext.setTextSize(size*(Float.parseFloat(read.getInt("textSizeModifier",10)+"f")/10f));
		button.setBackgroundDrawable(gd);
		buttontext.setShadowLayer(50,0,0,read.getInt("butshadow",Color.BLACK));
		buttontext.setTextColor(read.getInt("butcolor",Color.WHITE));
		i.setBackgroundColor(read.getInt("color",Color.BLACK));
	}
}
