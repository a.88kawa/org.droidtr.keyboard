package org.droidtr.keyboard;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class butchange extends Activity
 {

	@Override  
	protected void onCreate(Bundle savedInstanceState) {  
		super.onCreate(savedInstanceState);  
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.butchange);    
		SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
		Button onup=(Button)findViewById(R.id.onup);
		Button onmusic=(Button)findViewById(R.id.onmusic);
		Button onsound=(Button)findViewById(R.id.onsound);
		Button offup=(Button)findViewById(R.id.offup);
		Button offmusic=(Button)findViewById(R.id.offmusic);
		Button offsound=(Button)findViewById(R.id.offsound);
		Button offmenu=(Button) findViewById(R.id.offmenu);
		Button onctrlalt=(Button)findViewById(R.id.onctrlalt);
		Button on_off=(Button)findViewById(R.id.onoff);
		if(read.getBoolean("butfunc",false)){
			on_off.setTag("on");
		}
		else{
			on_off.setTag("off");
		}
		update(on_off);
		if(read.getString("onscreen","").contains("up")){
			onup.setEnabled(false);
		}else if(read.getString("onscreen","").contains("music")){
			onmusic.setEnabled(false);
		}else if(read.getString("onscreen","").contains("sound")){
			onsound.setEnabled(false);
		}else{
			onctrlalt.setEnabled(false);
		}
		if(read.getString("offscreen","").contains("up")){
			offup.setEnabled(false);
		}else if(read.getString("offscreen","").contains("music")){
			offmusic.setEnabled(false);
		}else if(read.getString("offscreen","").contains("menu")){
			offmenu.setEnabled(false);
		}else{
			offsound.setEnabled(false);
		}
	}
	public void onup(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableon(w);
		edit.putString("onscreen","up");
		edit.commit();
	}
	public void onmusic(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableon(w);
		edit.putString("onscreen","music");
		edit.commit();
	}
	public void onsound(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableon(w);
		edit.putString("onscreen","sound");
		edit.commit();
	}
	public void onctrlalt(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableon(w);
		edit.putString("onscreen","ctrlalt");
		edit.commit();
	}
	public void offup(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableoff(w);
		edit.putString("offscreen","up");
		edit.commit();
	}
	public void offmusic(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableoff(w);
		edit.putString("offscreen","music");
		edit.commit();
	}
	public void offsound(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableoff(w);
		edit.putString("offscreen","sound");
		edit.commit();
	}
	public void offmenu(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		enabledesableoff(w);
		edit.putString("offscreen","menu");
		edit.commit();
	}
	public void enabledesableoff(View w){
		Button up=(Button)findViewById(R.id.offup);
		Button music=(Button)findViewById(R.id.offmusic);
		Button sound=(Button)findViewById(R.id.offsound);
		Button menu=(Button)findViewById(R.id.offmenu);
		menu.setEnabled(true);
		up.setEnabled(true);
		music.setEnabled(true);
		sound.setEnabled(true);
		if(w!=null){
		w.setEnabled(false);
		}
	}
	public void enabledesableon(View w){
		Button up=(Button)findViewById(R.id.onup);
		Button music=(Button)findViewById(R.id.onmusic);
		Button sound=(Button)findViewById(R.id.onsound);
		Button ctrlalt=(Button)findViewById(R.id.onctrlalt);
		ctrlalt.setEnabled(true);
		up.setEnabled(true);
		music.setEnabled(true);
		sound.setEnabled(true);
		if(w!=null){
		w.setEnabled(false);
		}
	}
	public void onoff(View w){
		SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		
		if(w.getTag().toString().contains("on")){
			w.setTag("off");
			edit.putBoolean("butfunc",false);
		}else{
			w.setTag("on");
			edit.putBoolean("butfunc",true);
		}
		update(w);
	    edit.commit();
	}
	public void update(View w){
		LinearLayout ll1 = (LinearLayout)findViewById(R.id.ll1);
		LinearLayout ll2 = (LinearLayout)findViewById(R.id.ll2);
		LinearLayout ll3 = (LinearLayout)findViewById(R.id.ll3);
		LinearLayout ll4 = (LinearLayout)findViewById(R.id.ll4);	
		if(w.getTag().toString().contains("off")){
			ll1.setVisibility(View.GONE);
			ll2.setVisibility(View.GONE);
			ll3.setVisibility(View.GONE);
			ll4.setVisibility(View.GONE);
		}else{
			ll1.setVisibility(View.VISIBLE);
			ll2.setVisibility(View.VISIBLE);
			ll3.setVisibility(View.VISIBLE);
			ll4.setVisibility(View.VISIBLE);
		}
	}
}
