package org.droidtr.keyboard;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.inputmethodservice.*;
import android.inputmethodservice.KeyboardView.*;
import android.media.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.view.View.*;
import android.view.inputmethod.*;
import android.widget.*;
import java.io.*;
import java.security.*;
import java.util.*;
import org.xmlpull.v1.*;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ActionMenuView.*;
import java.util.concurrent.atomic.*;
import android.util.*;
//4299-4302 boşta
public class IME extends InputMethodService
implements OnKeyboardActionListener {
	private CustomKeyboardView kv;
	private LatinKeyboard keyboard;
	private boolean lastcaps=false;
	private boolean caps = false;
	private boolean ctrl = false;
	private boolean alt = false;
	private boolean capslock = false;
	private boolean vibenable = true;
	private boolean mute=true;
	private boolean dot = false;
	private int vibtime=25;
	private boolean otobuyuk=true; 
	private boolean otoparantez = true;
	private boolean emoji = false;
	private boolean suggestion=false;
	private boolean tabview = false;
	public String word="";
	public String tmp="";
	String val="";
	SharedPreferences read;
	SharedPreferences.Editor edit;
	Vibrator vib = null;
	AudioManager am = null;
	Random r= new Random();
	Handler h = new Handler();
	Runnable run= null;
	String key;
	public Drawable butback = null;
	public int butcolor=0;
	public int butshadow=0;
	public int i=0;
	LinearLayout.LayoutParams match_parrent = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT,1);
	public LinearLayout ll,lvl;
	public TextView[] b=new TextView[21];
	public TextView addbutton;
	private double modifier_portrait=1;
	private double modifier_landscape=1.5;
	private View emojiview;
	private int primaryColor;
	private int secondaryColor;
	private float radius;
	private int strokewidth;
	private int strokecolor;
	public boolean developer=true;

	private LinearLayout symlayout;

	private View suglayout;
	public void onText(CharSequence text)
	{}

	@Override
	public void requestHideSelf(int flags)
	{
		kv.closing();
		super.requestHideSelf(0);
	}


	public void initvalue(){ 
	    getBg();
		strokecolor=read.getInt("strokeColor",Color.TRANSPARENT);
		strokewidth=read.getInt("strokeWidth",5);
		radius=Float.parseFloat(read.getInt("radius",10)+"f");
	    primaryColor=read.getInt("primaryColor",Color.TRANSPARENT);
		secondaryColor=read.getInt("secondaryColor",Color.TRANSPARENT);
		butcolor = read.getInt("butcolor",Color.WHITE);
		butshadow = read.getInt("butshadow",Color.BLACK);
		suggestion=read.getBoolean("suggestion",false);
		tabview=read.getBoolean("tabview",false);
		emoji=read.getBoolean("emoji",true);
		read = getSharedPreferences("key",MODE_PRIVATE);
		vibtime = read.getInt("vibtime",25);
		vibenable= read.getBoolean("vibenable",true);
		otobuyuk = read.getBoolean("otobuyuk",true);
		otoparantez =read.getBoolean("otoparantez",true);
		mute=read.getBoolean("mute",false);
		modifier_portrait=((double)read.getInt("modifier_portrait",100))/100;
		modifier_landscape=((double)read.getInt("modifier_landscape",150))/100;
	}
	public LatinKeyboard getkeyboard(){
		read = getSharedPreferences("key",MODE_PRIVATE);
		val = read.getString("keyboard","trq2").toString();
		if (val.equals("tra")){
			keyboard = new LatinKeyboard(this, R.xml.tra);  
		}else if(val.equals("trq")){
			keyboard = new LatinKeyboard(this, R.xml.trq);
		}else if(val.equals("trf")){
			keyboard = new LatinKeyboard(this, R.xml.trf);
		}else if(val.equals("trf2")){
			developer=false;
			keyboard = new LatinKeyboard(this, R.xml.trf2);
		}else if(val.equals("aqvoid")){
			keyboard = new LatinKeyboard(this, R.xml.aqvoid);
		}else if(val.equals("trq2")){
			developer=false;
			keyboard = new LatinKeyboard(this, R.xml.trq2);
		}else{
			keyboard = new LatinKeyboard(this, R.xml.trq3);
			developer=false;
		}
		return keyboard;
	}
	public Drawable getBg(){
		if(read.getBoolean("activedraw",true)){
			int[] color= {primaryColor,secondaryColor};
			GradientDrawable gd = (GradientDrawable) getResources().getDrawable(R.drawable.shape);
			if(Build.VERSION.SDK_INT >= 16){
				gd.setColors(color);
				gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
			} else{
				gd.setColor(color[0]);
			}
			gd.setCornerRadius(radius);
			gd.setStroke(strokewidth,strokecolor);
			return gd;
		}else{
			return getResources().getDrawable(R.drawable.shape);
		}

	}

	@Override
	public View onCreateInputView() {
		try{
			ll =new LinearLayout(this);
			ll.setLayoutParams(new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
			ll.setOrientation(LinearLayout.VERTICAL);
			read = getSharedPreferences("key",MODE_PRIVATE);
			edit = getSharedPreferences("key",MODE_PRIVATE).edit();
			initvalue();
			kv = (CustomKeyboardView) getLayoutInflater().inflate(R.layout.keyboard, null);
			keyboard=getkeyboard();
			Locale locale = new Locale("tr", "TR");
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			this.getApplicationContext().getResources().updateConfiguration(config, null);
			keyboardpast(keyboard);
			kv.setOnKeyboardActionListener(this);
			kv.setBackgroundDrawable(null);
			if(read.getBoolean("activedraw",true)){
				if (new File(getFilesDir()+"/image.png").exists()){
					Drawable d = Drawable.createFromPath(Uri.fromFile(new File(getFilesDir()+"/image.png")).getPath());
					ll.setBackgroundDrawable(d);
				}else{
				ll.setBackgroundColor(read.getInt("color",Color.parseColor("#1a252f")));
				}
			}else{
				ll.setBackgroundColor(Color.parseColor("#1a252f"));
			}
			kv.setPreviewEnabled(read.getBoolean("preview",false));
			if(emoji){
				emojiview = new emoji(this).getLayout(
					new OnClickListener(){

						@Override
						public void onClick(View p1)
						{
							getCurrentInputConnection().commitText(((TextView)p1).getText().toString(),1);
						}
					},
					new OnClickListener(){

						@Override
						public void onClick(View p1)
						{
							onKey(-4226,null);
						}
					},
					new OnClickListener(){

						@Override
						public void onClick(View p1)
						{
							onKey(Keyboard.KEYCODE_DELETE,null);
						}
					});
			}
			if(emoji){
				ll.addView(emojiview);
				emojiview.setVisibility(View.GONE);
			}
		}catch(Exception e){}
		symlayout = getSymLayout();
		ll.addView(symlayout);
		if(!tabview){
		symlayout.setVisibility(View.GONE);
		}
		suglayout=getSuggestionLayout();
		ll.addView(suglayout);
		if(!suggestion){
			suglayout.setVisibility(View.GONE);
		}
		ll.addView(kv);
		return ll;
	}
	public String getAfterWord(InputConnection ic){
		String after="";
		try{
			after = ic.getTextAfterCursor(250,0).toString();
			after=after.split("\n")[0];
		}catch(IOError e){}
		catch(Error e){}
		catch(NullPointerException e){}
		catch(InflateException e){}
		catch(Exception e){}
		finally{}
		return after;
	}
	public String getBeforeWord(InputConnection ic){
		String before="";
		try{
			before=ic.getTextBeforeCursor(250,0).toString();
			before=before.split("\n")[before.split("\n").length-1];
		}catch(IOError e){}
		catch(Error e){}
		catch(NullPointerException e){}
		catch(InflateException e){}
		catch(Exception e){}
		finally{}
		return before;
	}
	public String getword(){
		String before = getBeforeWord(getCurrentInputConnection());
		String after= getAfterWord(getCurrentInputConnection());
		return (before.split(" ")[before.split(" ").length-1]).trim()+""+(after.split(" ")[0]).trim();
	}
	private LinearLayout getSuggestionLayout()
	{
		LinearLayout lhs = new LinearLayout(this);
		lvl = new LinearLayout(this);
		HorizontalScrollView hsv = new HorizontalScrollView(this);
		LinearLayout ls = new LinearLayout(this);
		try{
			addbutton = new TextView(this);
			addbutton.setBackgroundDrawable(getBg());
			addbutton.setShadowLayer(5,0,0,butshadow);
			addbutton.setTextColor(butcolor);
			addbutton.setPadding(10,10,10,10);
			addbutton.setTextSize(15*Float.parseFloat(read.getInt("textSizeModifier",10)+"f")/10f);
			addbutton.setGravity(Gravity.CENTER);
			addbutton.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View p1)
					{
						new suggestion().addSuggestion(word);
						updatesuggestion();
					}
				});
			addbutton.setOnLongClickListener(new OnLongClickListener(){

					@Override
					public boolean onLongClick(View p1)
					{
						suggestion=false;
						return false;
					}
				});
			lvl.addView(addbutton);
			for(int i=0; i!=100;i++){
				final int j=i;
				b[i]= new TextView(this);
				b[i].setTextSize(15*Float.parseFloat(read.getInt("textSizeModifier",10)+"f")/10f);
				b[i].setBackgroundDrawable(getBg());
				b[i].setTextColor(butcolor);
				b[i].setShadowLayer(5,0,0,butshadow);
				b[i].setGravity(Gravity.CENTER);
				b[i].setVisibility(View.GONE);
				b[i].setPadding(10,10,10,10);
				b[i].setOnClickListener(new OnClickListener(){
						@Override
						public void onClick(View p1)
						{
							String etiket =b[j].getText().toString().trim();
							String before =getBeforeWord(getCurrentInputConnection());
							word=before.split(" ")[before.split(" ").length-1];
							if(word.length()<etiket.length()){
								tmp=etiket.substring(word.length(),etiket.length());
								getCurrentInputConnection().commitText(tmp+" ",1);
							}
						}
					});
				b[i].setOnLongClickListener(new OnLongClickListener(){

						@Override
						public boolean onLongClick(View p1)
						{
							new suggestion().delSuggestion(b[j].getText().toString());
							return false;
						}
					});
				ls.addView(b[i]);
			}

		}catch(Exception e){}
		lvl.addView(lhs);
		lhs.addView(hsv);
		hsv.addView(ls);
		return lvl;
	}
	
	private LinearLayout getSymLayout()
	{
		final String sym="{}();,.=\\\"'|&![]<>+-/*?%~#@$`:_";
		TextView[] t= new TextView[sym.length()];
		LinearLayout lhs = new LinearLayout(this);
		lvl = new LinearLayout(this);
		HorizontalScrollView hsv = new HorizontalScrollView(this);
		LinearLayout ls = new LinearLayout(this);
			for(int i=1; i!=sym.length();i++){
				final int j=i;
				t[i]= new TextView(this);
				t[i].setTextSize(15*Float.parseFloat(read.getInt("textSizeModifier",10)+"f")/10f);
				t[i].setBackgroundDrawable(getBg());
				t[i].setTextColor(butcolor);
				t[i].setTypeface(null,Typeface.BOLD);
				t[i].setText("   "+sym.split("")[i]+"   ");
				t[i].setShadowLayer(5,0,0,butshadow);
				t[i].setGravity(Gravity.CENTER);
				t[i].setPadding(10,10,10,10);
				t[i].setOnClickListener(new OnClickListener(){
						@Override
						public void onClick(View p1)
						{
							getCurrentInputConnection().commitText(sym.split("")[j].trim(),1);
							complate(sym.split("")[j].trim());
						}
					});
				ls.addView(t[i]);
			}
		lvl.addView(lhs);
		lhs.addView(hsv);
		hsv.addView(ls);
		return lvl;
	}
	@Override
	public boolean onShowInputRequested(int flags, boolean configChange)
	{
		updatesuggestion();
		return super.onShowInputRequested(flags, configChange);
	}

	public KeyEvent setctrlalt(int keycode,boolean ctrl,boolean alt, boolean caps){
		if(!caps){
			if(ctrl){
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON |KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else{
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,0,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		}else{
			if(ctrl){  
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
				else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_CTRL_ON | KeyEvent.META_SHIFT_ON ,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}else {
				if(alt){
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_ALT_ON | KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				} else{
					return new KeyEvent(SystemClock.uptimeMillis(),SystemClock.uptimeMillis(),KeyEvent.ACTION_DOWN,keycode,0,KeyEvent.META_SHIFT_ON,0,0,KeyEvent.FLAG_SOFT_KEYBOARD | KeyEvent.FLAG_KEEP_TOUCH_MODE,InputDevice.SOURCE_KEYBOARD);
				}
			}
		}

	}

	@Override
	public void onKey(int primaryCode, int[] keyCodes) {
		key="";
		keypast(primaryCode);
		try{
			if(primaryCode!=0 && this.isInputViewShown()){
				if(suggestion &&( primaryCode==32 || primaryCode == Keyboard.KEYCODE_DONE)){
					new suggestion().addSuggestion(addbutton.getText().toString());
				}
				if(caps){
					if(primaryCode==287){
						primaryCode=286;

					}else if (primaryCode==351){
						primaryCode=350;
					}
				}
				if (primaryCode != 32 && primaryCode != 46){
					dot=false;
				}

				//getCurrentInputConnection().commitText(Integer.toString( primaryCode),1);
				switch(primaryCode){
					case 0:
						getCurrentInputConnection().commitText("",1);
						break;
					case 19:
						swipeUp();
						break;
					case 20:
						swipeDown();
						break;
					case Keyboard.KEYCODE_DELETE :
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_DEL,ctrl,alt,false));
						if(!capslock){
							caps=false;
						}
						break;
					case Keyboard.KEYCODE_SHIFT:
						if (caps){
							capslock=!capslock;
						}if(!capslock){
							caps = !caps;
						}
						break;
					case Keyboard.KEYCODE_DONE:
						primaryCode=KeyEvent.KEYCODE_ENTER;
						sendDownUpKeyEvents(primaryCode);
						if(otobuyuk){
							caps=true;
							capslock=false;
						}
						break;
					case -4128:
						keyboard = new LatinKeyboard(this,R.xml.trf);  
						keyboardpast(keyboard);
						setkeyboard("trf");
						developer=true;
						break;
					case -4129:
						keyboard = new LatinKeyboard(this,R.xml.trq);
						keyboardpast(keyboard);
						setkeyboard("trq");
						developer=true;
						break;
					case -4130:
						if(developer){
							keyboard = new LatinKeyboard(this,R.xml.sym);
						}else{
							keyboard = new LatinKeyboard(this,R.xml.sym2);
						}
						keyboardpast(keyboard);
						break;
					case -4131: 
						keyboard = new LatinKeyboard(this,R.xml.num);
						keyboardpast(keyboard);
						break;
					case -4132:
						keyboard = new LatinKeyboard(this,R.xml.tra);
						keyboardpast(keyboard);
						setkeyboard("tra");
						developer=true;
						break;
					case -4304:
						keyboard = new LatinKeyboard(this,R.xml.aqvoid);
						keyboardpast(keyboard);
						setkeyboard("aqvoid");
						developer=true;
						break;
					case -4133:
						edit.putBoolean("mute",!mute);  
						edit.commit();
						mute=!mute;
						if(mute){
							playClick(primaryCode);
						}
						break;  
					case -4135:
						keyboard = new LatinKeyboard(this,R.xml.none);
						keyboardpast(keyboard);
						emojiview.setVisibility(View.VISIBLE);
						symlayout.setVisibility(View.GONE);
						suglayout.setVisibility(View.GONE);
						break;
					case -4215:
						keyboard = new LatinKeyboard(this,R.xml.grk);
						keyboardpast(keyboard);
						break;
					case -4216:
						keyboard = new LatinKeyboard(this,R.xml.grk2);
						keyboardpast(keyboard);
						break;
					case -4226:
						keyboard=getkeyboard();
						keyboardpast(keyboard);
						emojiview.setVisibility(View.GONE);
						if(tabview){
							symlayout.setVisibility(View.VISIBLE);
						}if(suggestion){
							suglayout.setVisibility(View.VISIBLE);
						}
						break;
					case -4227:
						keyboard = new LatinKeyboard(this,R.xml.settings);
						capslock=false;
						caps=false;
						keyboardpast(keyboard);
						break;
					case -4228:
						keyboard = new LatinKeyboard(this,R.xml.trf2);  
						keyboardpast(keyboard);
						setkeyboard("trf2");
						developer=false;
						break;
					case -4236:
						edit.putInt("vibtime",25);  
						vibtime=25;
						edit.commit();
						playvibrator();
						break;    
					case -4234:  
						edit.putInt("vibtime",50);  
						edit.commit();
						vibtime=50;
						playvibrator();
						break;
					case -4235:  
						edit.putInt("vibtime",75);  
						edit.commit();
						vibtime=75;
						playvibrator();
						break;  
					case -4232:
						swipeRight();
						break; 
					case -4233:  
						swipeLeft();
						break;
					case -4298:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_ESCAPE,ctrl,alt,caps));
						break;
					case -4306:
						keyboard = new LatinKeyboard(this,R.xml.trq3);
						keyboardpast(keyboard);
						setkeyboard("trq3");
						developer=false;
						break;
					case -4308:
						boolean preview = read.getBoolean("preview",false);  
						edit.putBoolean("preview",!preview);
						kv.setPreviewEnabled(!preview);
						edit.commit();
						break;
					case -4309:
						keyboard = new LatinKeyboard(this,R.xml.trq2);  
						keyboardpast(keyboard);
						setkeyboard("trq2");
						developer=false;
						break;
					case -4311 :
						suggestion=!suggestion;
						edit.putBoolean("suggestion",suggestion);
						edit.commit();
						if(suggestion){
							suglayout.setVisibility(View.VISIBLE);
						}else{
							suglayout.setVisibility(View.GONE);
						}
						break;
					case -4312 :
						tabview=!tabview;
						edit.putBoolean("tabview",tabview);
						edit.commit();
						if(tabview){
							symlayout.setVisibility(View.VISIBLE);
						}else{
							symlayout.setVisibility(View.GONE);
						}
						break;
					case -4313:
						edit.putInt("vibtime",100);  
						edit.commit();
						vibtime=100;
						playvibrator();
						break;
					case -4314:
						vibenable= !vibenable;
						edit.putBoolean("vibenable",vibenable);  
						edit.commit();
						playvibrator();
						break;  
					case -4315:
						otobuyuk= !otobuyuk;
						edit.putBoolean("otobuyuk",otobuyuk);  
						edit.commit();
						break; 
					case -4316:
						ctrl=!ctrl;
						break;
					case -4317:
						alt=!alt;
						break;
					case -4318:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_MENU,ctrl,alt,caps));
						break;
					case -4319:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_VOLUME_UP,ctrl,alt,caps));
						break;
					case -4320:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_MEDIA_NEXT,ctrl,alt,caps));
						break;
					case -4321:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_MEDIA_PREVIOUS,ctrl,alt,caps));
						break;
					case -4322:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(85,ctrl,alt,caps));
						break;
					case -4323:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(86,ctrl,alt,caps));
						break;
					case -4324:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_PAGE_UP,ctrl,alt,caps));
						break;
					case -4325:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_PAGE_DOWN,ctrl,alt,caps));
						break;
					case -4326:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(122,ctrl,alt,caps));
						break;
					case -4327:
						getCurrentInputConnection().sendKeyEvent(setctrlalt(123,ctrl,alt,caps));
						break;
					default:
							char code = (char)primaryCode;
							if(Character.isLetter(code) && caps){ 
								key = Character.toString(code).toUpperCase(new Locale("tr","TR"));
								if(!capslock){
									caps=false;
								}
							}else{
								key=Character.toString(code);
							}
							if (ctrl || alt){
								getCurrentInputConnection().sendKeyEvent(setctrlalt(primaryCode-68,ctrl,alt,caps));
								ctrl=false;
								alt=false;
							}else{
								getCurrentInputConnection().commitText(key,1);
							}
							complate(key);
						
				}
				if(primaryCode==46){
					dot=true;
				}
				if(primaryCode==32 && dot && otobuyuk){
					dot=!dot;
					caps=true;
					capslock=false;
					
				}
				if(lastcaps !=caps){
					lastcaps=caps;
					kv.setShifted(caps);
					kv.invalidateAllKeys();
				}
				updatesuggestion();
			}
		}catch(Exception e){}
	}
	public void complate(String key){
		if(otoparantez){
			if(key.equals("\"")){
				getCurrentInputConnection().commitText(key,1);
				swipeLeft();
			}else if(key.equals("'")){
				getCurrentInputConnection().commitText(key,1);
				swipeLeft();
			}else if(key.equals("(")){
				getCurrentInputConnection().commitText(")",1);
				swipeLeft();
			}
			else if(key.equals("[")){
				getCurrentInputConnection().commitText("]",1);
				swipeLeft();
			}
			else if(key.equals("{")){
				getCurrentInputConnection().commitText("}",1);
				swipeLeft();
			}
			else if(key.equals("<")){
				getCurrentInputConnection().commitText(">",1);
				swipeLeft();
			}
			else if(key.equals("`")){
				getCurrentInputConnection().commitText(key,1);
				swipeLeft();
			}
		}
	}
	

	private void updatesuggestion()
	{
		if(suggestion){
		try{
			word=getword();
			addbutton.setText(word);
						String[] sugarray= new suggestion().getSuggestionArray(word);
						for(int i=0;i!=20;i++){
							b[i].setVisibility(View.GONE);
						}
						for(int i=1;i!=sugarray.length || i!=20;i++){
							b[i].setText(sugarray[i]);
							b[i].setVisibility(View.VISIBLE);
						}
					
		}catch(Exception e){}
		}
	}

	public void setkeyboard(String layout){
		final SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		edit.putString("keyboard",layout);
		edit.commit();
	}
	public void sendchar(char c,boolean caps){
		char code = c;
		if(c=='<'&&caps){
			key=">";
		}else if(c=='.'&&caps){
			key=":";
		}else if(c==','&&caps){
			key=";";
		}
		else if(Character.isLetter(code) && caps){ 

			key = Character.toString(code).toUpperCase(new Locale("tr","TR"));
			keyboard.setShifted(caps); 
			kv.invalidateAllKeys(); 
		}else{
			key=Character.toString(code);
		}
		getCurrentInputConnection().commitText(key,1);
	}

	public void keypast(final int primaryCode){
		if(primaryCode !=0 && this.isInputViewShown()){
			h.post(new Runnable(){

					@Override
					public void run()
					{
						if(mute){
							playClick(primaryCode);
						}
						if(vibenable){
							playvibrator();
						}
					}

				});
		}
	}
	private void playClick(int keyCode){
		try{
			am = (AudioManager)getSystemService(AUDIO_SERVICE);
			switch(keyCode){
				case 32: 
					am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
					break;
				case 10: 
					am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
					break;
				case Keyboard.KEYCODE_DELETE:
					am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
					break;
				case 227:
					am.playSoundEffect(AudioManager.FX_KEY_CLICK);
					break;
				default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
			}
		}catch(Exception e){}
	}
	public void playvibrator(){  
		try{
			vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			vib.vibrate(vibtime);
		}catch(Exception e){}
	}
	@Override
	public void onPress(int primaryCode) {
	}

	@Override
	public void onRelease(int primaryCode) { 
	}


	@Override
	public void swipeDown() {
		getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_DPAD_DOWN,ctrl,alt,caps));
	}

	@Override
	public void swipeLeft() {
		getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_DPAD_LEFT,ctrl,alt,caps));
	}

	@Override
	public void swipeRight() {
		getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_DPAD_RIGHT,ctrl,alt,caps));
	}

	@Override
	public void swipeUp() {
		getCurrentInputConnection().sendKeyEvent(setctrlalt(KeyEvent.KEYCODE_DPAD_UP,ctrl,alt,caps));
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{

		try{
			SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
			if(read.getBoolean("butfunc",false)){
				keypast(keyCode);
				//ic.commitText(Integer.toString(keyCode),1);
				if(keyCode==32 && suggestion){
					new suggestion().addSuggestion(addbutton.getText().toString());
				}
				if(this.isInputViewShown() && this.isShowInputRequested()){
					if (keyCode==KeyEvent.KEYCODE_VOLUME_UP){  
						if(read.getString("onscreen","").contains("sound")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_VOLUME_UP);
						}else if(read.getString("onscreen","").contains("music")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_MEDIA_NEXT);
						}else if(read.getString("onscreen","").contains("up")){
							if(caps){
								caps=false;
								swipeLeft();
								caps=true;
							}else{
								swipeUp();
							}
						}else{
							ctrl=!ctrl;
						}
						return true;
					}else if (keyCode==KeyEvent.KEYCODE_VOLUME_DOWN ){
						if(read.getString("onscreen","").contains("sound")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_VOLUME_DOWN);
						}else if(read.getString("onscreen","").contains("music")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_MEDIA_PREVIOUS);
						}else if(read.getString("onscreen","").contains("ctrlalt")){
							alt=!alt;
						}else{
							if(caps){
								caps=false;
								swipeRight();
								caps=true;
							}else{
								swipeDown();
							}
						}
						return true;
					}
				}else{
					if (keyCode==KeyEvent.KEYCODE_VOLUME_UP ){  
						if(read.getString("offscreen","").contains("up")){
							swipeUp();
						}else if(read.getString("offscreen","").contains("music")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_MEDIA_NEXT);
						}else if(read.getString("offscreen","").contains("menu")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_MENU);
						}else{
							sendDownUpKeyEvents(KeyEvent.KEYCODE_VOLUME_UP);
						}
						return true;
					}else if (keyCode==KeyEvent.KEYCODE_VOLUME_DOWN ){
						if(read.getString("offscreen","").contentEquals("up")){
							swipeDown();
						}else if(read.getString("offscreen","").contains("music")){
							sendDownUpKeyEvents(KeyEvent.KEYCODE_MEDIA_PREVIOUS);
						}else if(read.getString("offscreen","").contains("menu")){
							showWindow(true);
						}else{
							sendDownUpKeyEvents(KeyEvent.KEYCODE_VOLUME_DOWN);
						}
						return true;
					}
				}
				if(keyCode==KeyEvent.KEYCODE_BACK){	
					return super.onKeyDown(keyCode,event);
				}else if(keyCode == 79){
					getCurrentInputConnection().sendKeyEvent(setctrlalt( KeyEvent.KEYCODE_CAMERA,false,false,false));
				}
				else if(keyCode==KeyEvent.KEYCODE_DEL){
					getCurrentInputConnection().sendKeyEvent(setctrlalt(keyCode,ctrl,alt,caps));
					return true;
				}
				else{
					if ((read.getString("keyboard","trq2").toString().equals("trf") || read.getString("keyboard","trq2").toString().equals("trf2"))){
						try{
							if(keyCode==KeyEvent.KEYCODE_E){
								sendchar('ğ',(event.isShiftPressed()));
							}else if(keyCode==KeyEvent.KEYCODE_S){
								sendchar('i',(event.isShiftPressed()));
							}else if(keyCode==KeyEvent.KEYCODE_R){
								sendchar('ı',(event.isShiftPressed()));
							}else if(keyCode==KeyEvent.KEYCODE_G){
								sendchar('ü',(event.isShiftPressed()));
							}else if(keyCode==75){
								sendchar('ş',(event.isShiftPressed()));
							}else if(keyCode==81){
								sendchar('<',(event.isShiftPressed()));
							}else if(keyCode==KeyEvent.KEYCODE_B){
								sendchar('ç',(event.isShiftPressed()));
							}else if(keyCode==73){
								sendchar('.',(event.isShiftPressed()));
							}else if(keyCode==56){
								sendchar(',',(event.isShiftPressed()));
							}else if(keyCode==KeyEvent.KEYCODE_X){
								sendchar('ö',(event.isShiftPressed()));
							}  
							else{
								getCurrentInputConnection().sendKeyEvent(new KeyEvent(
																			 event.getDownTime(), 
																			 event.getEventTime(), 
																			 event.getAction(), 
																			 qtof(keyCode), 
																			 event.getRepeatCount(), 
																			 event.getMetaState(), 
																			 event.getDeviceId(), 
																			 event.getScanCode(), 
																			 event.getFlags(),
																			 event.getSource()
																		 ));

							}
						}catch(IOError e){
						}}else{
						super.onKeyDown(keyCode,event);
					}

				}
			}else{
				return super.onKeyDown(keyCode,event);
			}
				
		
		}catch(IOError e){}
		catch(Error e){}
		catch(Exception e){}
		finally{} 
		return true;
	}
	public int qtof(int key){
		int i =key;
		switch (key){
			case KeyEvent.KEYCODE_Q:
				i= KeyEvent.KEYCODE_F;
				break;
			case KeyEvent.KEYCODE_W:
				i= KeyEvent.KEYCODE_G;
				break;
			case KeyEvent.KEYCODE_R:
				i= KeyEvent.KEYCODE_I;
				break;
			case KeyEvent.KEYCODE_T:
				i= KeyEvent.KEYCODE_O;
				break;
			case KeyEvent.KEYCODE_Y:
				i= KeyEvent.KEYCODE_D;
				break;
			case KeyEvent.KEYCODE_U:
				i= KeyEvent.KEYCODE_R;
				break;
			case KeyEvent.KEYCODE_I:
				i= KeyEvent.KEYCODE_N;
				break;
			case KeyEvent.KEYCODE_O:
				i= KeyEvent.KEYCODE_H;
				break;
			case 71:
				i= KeyEvent.KEYCODE_Q;
				break;
			case 72:
				i= KeyEvent.KEYCODE_W;
				break;

			case KeyEvent.KEYCODE_A:
				i= KeyEvent.KEYCODE_U;
				break;
			case KeyEvent.KEYCODE_D:
				i= KeyEvent.KEYCODE_E;
				break;
			case KeyEvent.KEYCODE_F:
				i= KeyEvent.KEYCODE_A;
				break;
			case KeyEvent.KEYCODE_H:
				i= KeyEvent.KEYCODE_T;
				break;
			case KeyEvent.KEYCODE_J:
				i= KeyEvent.KEYCODE_K;
				break;
			case KeyEvent.KEYCODE_K:
				i= KeyEvent.KEYCODE_M;
				break;
			case 74:
				i= KeyEvent.KEYCODE_Y;
				break;
			case 52:
				i= KeyEvent.KEYCODE_J;
				break;
			case KeyEvent.KEYCODE_C:
				i= KeyEvent.KEYCODE_V;
				break;
			case KeyEvent.KEYCODE_V:
				i= KeyEvent.KEYCODE_C;
				break;
			case KeyEvent.KEYCODE_N:
				i= KeyEvent.KEYCODE_Z;
				break;
			case KeyEvent.KEYCODE_M:
				i= KeyEvent.KEYCODE_S;
				break;
			case 70:
				i= KeyEvent.KEYCODE_B;
				break;
			case 55:
				i= KeyEvent.KEYCODE_X;
				break;
			case KeyEvent.KEYCODE_Z:
				i= KeyEvent.KEYCODE_J;
				break;
		}
		return i;
	}

	@Override
	public void onWindowHidden()
	{
		keyboard = new LatinKeyboard(this,R.xml.none);
		keyboardpast(keyboard);
		System.gc();
		System.runFinalization();
		if(read.getBoolean("lowmemory",false)){
			System.exit(0);
		}else{
			super.onWindowHidden();
			initvalue();
		}
		suggestion=false;
		onKey(0,null);
	}


	@Override
	public void onWindowShown()
	{
		try{
			if(read.getBoolean("exitrequest",false)){
				edit.putBoolean("exitrequest",false);
				edit.commit();
				System.exit(0);
			}
			keyboard=getkeyboard();
			keyboardpast(keyboard);
			if(emojiview !=null){
				emojiview.setVisibility(View.GONE);
			}
			suggestion=read.getBoolean("suggestion",false);
		}catch(Exception e){}
		super.onWindowShown();
	}
	public void keyboardpast(LatinKeyboard keyboard){
		try{
			if(!emoji){
				for(Keyboard.Key key :keyboard.getKeys()){
					if(key.codes[0]==4135){
						key.x+=key.width/2;
						key.y+=key.height/2;
						key.height=0;
						key.width=0;
					}
				}
			}for(Keyboard.Key key :keyboard.getKeys()){
				if(!key.sticky){
					key.height=key.height-2;
					key.y=key.y+1;
					key.width=key.width-2;
					key.x=key.x+1;
				}else{
					key.label=" ";
				}
			}
			if(getResources().getConfiguration().orientation == getResources().getConfiguration().ORIENTATION_PORTRAIT){
				keyboard.changeKeyHeight(modifier_portrait);
			}else{
				keyboard.changeKeyHeight(modifier_landscape);
			}
			kv.setKeyboard(keyboard);
			kv.closing();
			kv.invalidateAllKeys();
		}
		catch(IOError e){}
		catch(Error e){}
		catch(NullPointerException e){}
		catch(InflateException e){}
		catch(Exception e){}
		finally{}
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event)
	{
		return true;
	}

	@Override
	public View onCreateCandidatesView()
	{
		return null;
		//return super.onCreateCandidatesView();
	}
}

