package org.droidtr.keyboard;

import java.io.*;
import java.util.*;
import java.nio.charset.*;

public class suggestion
 {
    public String s[] = {};
	public String list="";
	public String path="/data/data/org.droidtr.keyboard/files";
	public String suggestion;
	public File flist;
	public Scanner sc;
	public String tmp;
	public String line;
	public FileOutputStream fos;
	public DataOutputStream dos;
	
	public String[] getSuggestionArray(String a){
		a=a.trim();
		String tmp[]= getSuggestion(a).split(",");
		for (int i = 0;i != tmp.length;i++)
		{
			tmp[i]=deconv(tmp[i]).trim();
		}
		return tmp;
	}
	public String getSuggestion(String a){
		new File(path).mkdirs();
		suggestion="";
		a=conv(a);
		if(a.length()<3){
			suggestion="";
		}else{
			s = a.split("");
			list=convpath(s[1]+s[2]+s[3]);
			flist =new File(path+"/"+list);
			try
			{
				sc = new Scanner(flist);
				if (flist.exists())
				{
					suggestion = ",";
					tmp="\n";
					while(sc.hasNextLine()){
						tmp=sc.nextLine();
						if(tmp.contains(a)){
							suggestion = suggestion+tmp;
							if(sc.hasNextLine()){
								suggestion=suggestion+",";
							}
						}
					}
					suggestion=suggestion.replaceAll(",,","");
				}else{
					flist.createNewFile();	
				}
			}catch (IOException e){}	
		}
		return suggestion;
	}

	public int addSuggestion(String a){
		/*
		 0 başarılı
		 1 kelime kısa
		 2 IOException
		 */
		a=conv(a);
		if(a.length()>=3){
			s = a.split("");
			list=convpath(s[1]+s[2]+s[3]);	
			try{
					flist = new File(path+"/"+list);
					tmp="\n"+a.trim();
					if(flist.exists()){
						sc = new Scanner(flist);		
						while(sc.hasNextLine()){
							line=sc.nextLine().trim();
							if(line.length()>=3 && !line.equals(a)){
								tmp=tmp+"\n"+line;
							}
						}
					}else{
						flist.createNewFile();
					}
					fos =new FileOutputStream(path+"/"+list);
					dos = new DataOutputStream(fos);
					dos.writeUTF(tmp);
					dos.flush();
			}catch (IOException e){
				return 2;
			}
			return 0;
		}else{
			return 1;
		}

	}
	public String conv(String msg){
		msg=msg.replaceAll("&","&a;");
		msg=msg.replaceAll(",","&v;");
		msg=msg.replaceAll("/","&b;");
		return msg.trim();
	}
	public String deconv(String msg){
		msg=msg.replaceAll("&a;","&");
		msg=msg.replaceAll("&v;",",");
		msg=msg.replaceAll("&b;","/");
		return msg.trim();
	}
	public String convpath(String msg){
		byte[] tmp=msg.getBytes(Charset.defaultCharset());
		msg="msg";
		for(int i=0;i!=tmp.length;i++){
			msg=msg+"-"+((int)tmp[i]);
			msg=msg.replaceAll("--","-");
		}
		return msg.trim();
	}
	public int delSuggestion(String a){
		/*
		 0 başarılı
		 1 kelime kısa
		 2 IOException
		 */
		a=conv(a);
		if(a.length()>=3){
			s= a.split("");
			list=convpath(s[1]+s[2]+s[3]);	
			try{		
					flist = new File(path+"/"+list);
					tmp="\n";
					if(flist.exists()){
						sc = new Scanner(flist);		
						while(sc.hasNextLine()){
							line=sc.nextLine().trim();
							if(line.length()>=3 && !line.equals(a)){
								tmp=tmp+line+"\n";
							}
						}
					}else{
						flist.createNewFile();
					}
					fos =new FileOutputStream(path+"/"+list);
					dos = new DataOutputStream(fos);
					dos.writeUTF(tmp);
					dos.flush();
			}catch (IOException e){
				return 2;
			}
			return 0;
		}else{
			return 1;
		}

	}
}
