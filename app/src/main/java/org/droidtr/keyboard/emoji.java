package org.droidtr.keyboard;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

public class emoji {

	Context c;
	View[] tabs;
	int selected = 0;
	LinearLayout et,ml;
	View.OnClickListener o;
	boolean start = true;
	String[][] emojis = new String[][]{
		{
			"☺","👅","😀","😁","😂","😃","😄","😅","😆","😇",
			"😈","😉","😊","😋","😌","😍","😎","😏","😐","😑",
			"😒","😓","😔","😕","😖","😗","😘","😙","😚","😛",
			"😜","😝","😞","😟","😠","😡","😢","😣","😤","😥",
			"😦","😧","😨","😩","😪","😫","😬","😭","😮","😯",
			"😰","😱","😲","😳","😴","😵","😶","😷","☝","✊",
			"✋","✌","👆","👇","👈","👉","👊","👋","👌","👍",
			"👎","👏","👐","🙌","🙏","💋","💌","💍","💎","💏",
			"💐","💑","💒","💓","💔","💕","💖","💗","💘","❤️"
			,"💙","💚","💛","💜","💝","💞","💟","🏂","🏃","🏄",
			"🏇","🏊","👤","👥","👦","👧","👨","👩","👪","👫",
			"👬","👭","👮","👯","👰","👱","👲","👳","👴","👵",
			"👶","👷","👸","👹","👺","👻","👼","👽","👾","👿",
			"💀","💁","💂","💃","💆","💇","💈","🙅","🙆","🙇",
			"🙊","🙋","🙍","🙎","🚴","🚵","🚶"
		},{
			"©","®","‼","⁉","™","ℹ","Ⓜ","♻","⚠","⚡","⛔",
			"📵","🔇","🔉","🔊","🔕","🔯","🔱","🚫","🚮","🚯",
			"🚰","🚱","🚳","🚷","🚸","🛂","🛃","🛄","🛅",
			"↔","↕","↖","↗","↘","↙","↩","↪","⏩","⏪","⏫",
			"⏬","▶","◀","➡","⤴","⤵","⬅","⬆","⬇","🔀","🔁",
			"🔂","🔃","🔄","🔼","🔽"
		},{
			"⌚","⌛","⏰","⏳","☎","✂","✅","✉","✏","✒",
			"🌂","🎒","🎓","🎣","🎤","🎥","🎦","🎧","🎨","🎩",
			"🎭","🎮","🎰","🎲","🎳","🎴","🎵","🎶","🎷","🎸",
			"🎹","🎺","🎻","🎼","🎽","🎾","🎿","🏀","🏁","🏆",
			"🏈","🏉","🏠","🏡","🏧","🏮","👑","👒","👔","👕",
			"👗","👘","👙","👚","👛","👜","👝","👞","👟","👠",
			"👡","👢","👣","💄","💅","💉","💊","💠","💡","💢",
			"💣","💤","💰","💲","💴","💵","💺","💻","💼","💽",
			"💾","💿","📀","📖","📝","📞","📠","📡","📢","📣",
			"📨","📩","📪","📫","📬","📭","📮","📯","📰","📱",
			"📲","📳","📴","📶","📷","📹","📺","📻","📼","🔈",
			"🔍","🔎","🔏","🔐","🔑","🔒","🔓","🔔","🔞","🔥",
			"🔦","🔧","🔨","🔩","🔪","🔫","🔬","🔭","🔮","🚀",
			"🚥","🚦","🚧","🚩","🚬","🚭","🚲","🚹","🚺","🚻",
			"🚼","🚽","🚾","🛀"
		},{
			"▪","▫","◻","◼","◽","◾","☑","♠","♣","♥","♦",
			"♿","⚪","⚫","⚽","⚾","⛄","⛵","✔","✖","✨",
			"✳","✴","❇","❌","❎","❓","❔","❕","❗","❤",
			"➕","➖","➗","➰","➿","⬛","⬜","⭐","⭕","〰",
			"〽","㊗","㊙","🀄","🃏","🎀","🎁","🎃","🎄","🎅",
			"🎆","🎇","🎈","🎉","🎊","🎋","🎌","🎍","🎎","🎏",
			"🎐","🎠","🎡","🎢","🎫","🎬","🎯","🎱","👀","👂",
			"👃","👄","💨","💩","🔅","🔆","🔋","🔌","🔖","🔗",
			"🔘","🔲","🔳","🔴","🔵","🔶","🔷","🔸","🔹","🔺",
			"🔻","🚨","🚪","🚿","🛁","♈","♉","♊","♋","♌",
			"♍","♎","♏","♐","♑","♒","♓"
		},{
			"☕","♨","🌰","🍅","🍆","🍇","🍈","🍉","🍊","🍋",
			"🍌","🍍","🍎","🍏","🍐","🍑","🍒","🍓","🍔","🍕",
			"🍖","🍗","🍘","🍙","🍚","🍛","🍜","🍝","🍞","🍟",
			"🍠","🍡","🍢","🍣","🍤","🍥","🍦","🍧","🍨","🍩",
			"🍪","🍫","🍬","🍭","🍮","🍯","🍰","🍱","🍲","🍳",
			"🍴","🍵","🍶","🍷","🍸","🍹","🍺","🍻","🍼","🎂"
		},{
			"⚓","⛅","⛎","🐋","👓","👖","💥","💪","💫","💬",
			"💭","💮","💯","💱","💳","💶","💷","💸","💹","📁",
			"📂","📃","📄","📅","📆","📇","📈","📉","📊","📋",
			"📌","📍","📎","📏","📐","📑","📒","📓","📔","📕",
			"📗","📘","📙","📚","📛","📜","📟","📤","📥","📦","📧"
		},{
			"⛪","⛲","⛳","⛺","⛽","🌍","🌎","🌏","🌐","🎪",
			"🏢","🏣","🏤","🏥","🏦","🏨","🏩","🏪","🏫","🏬",
			"🏭","🏯","🏰","🗻","🗼","🗽","🗾","🗿","✈","🚁",
			"🚂","🚃","🚄","🚅","🚆","🚇","🚈","🚉","🚊","🚋",
			"🚌","🚍","🚎","🚏","🚐","🚑","🚒","🚓","🚔","🚕",
			"🚖","🚗","🚘","🚙","🚚","🚛","🚜","🚝","🚞","🚟",
			"🚠","🚡","🚢","🚣","🚤"
		},{
			"🅰","🅱","🅾","🅿","🆎","🆑","🆒","🆓","🆔","🆕","🆖",
			"🆗","🆘","🆙","🆚","🔙","🔚","🔛","🔜","🔝","🔟",
			"🔠","🔡","🔢","🔣","🔤","#⃣","0⃣","1⃣","2⃣","3⃣",
			"4⃣","5⃣","6⃣","7⃣","8⃣","9⃣","🕐","🕑","🕒","🕓",
			"🕔","🕕","🕖","🕗","🕘","🕙","🕚","🕛","🕜","🕝",
			"🕞","🕟","🕠","🕡","🕢","🕣","🕤","🕥","🕦","🕧",
			"🈁","🈂","🈚","🈯","🈲","🈳","🈴","🈵","🈶","🈷",
			"🈸","🈹","🈺","🉐","🉑","🔰"
		},{
			"🐀","🐁","🐂","🐃","🐄","🐅","🐆","🐇","🐈","🐉",
			"🐊","🐌","🐍","🐎","🐏","🐐","🐑","🐒","🐓","🐔",
			"🐕","🐖","🐗","🐘","🐙","🐚","🐛","🐜","🐝","🐞",
			"🐟","🐠","🐡","🐢","🐣","🐤","🐥","🐦","🐧","🐨",
			"🐩","🐪","🐫","🐬","🐭","🐮","🐯","🐰","🐱","🐲",
			"🐳","🐴","🐵","🐶","🐷","🐸","🐹","🐺","🐻","🐼",
			"🐽","🐾","😸","😹","😺","😻","😼","😽","😾","😿",
			"🙀","🙈","🙉","🌱","🌲","🌳","🌴","🌵","🌷","🌸",
			"🌹","🌺","🌻","🌼","🌽","🌾","🌿","🍀","🍁","🍂",
			"🍃","🍄","☀","☁","☔","❄","🌀","🌁","🌃","🌄",
			"🌅","🌆","🌇","🌈","🌉","🌊","🌋","🌌","🌑","🌒",
			"🌓","🌔","🌕","🌖","🌗","🌘","🌙","🌚","🌛","🌜",
			"🌝","🌞","🌟","🌠","🎑","💦","💧"
		},{
			"🇹🇷","🇦🇿","🇨🇳","🇩🇪","🇪🇸","🇫🇷","🇬🇧","🇮🇹","🇯🇵","🇰🇷","🇷🇺",
			"🇺🇸","🇦🇺","🇦🇹","🇧🇪","🇧🇷","🇨🇦","🇨🇱","🇨🇴","🇩🇰","🇫🇮",
			"🇭🇰","🇮🇳","🇮🇩","🇮🇪","🇮🇱","🇲🇴","🇲🇾","🇲🇽","🇳🇱","🇳🇿",
			"🇳🇴","🇵🇭","🇵🇱","🇵🇹","🇵🇷","🇸🇦","🇸🇬","🇿🇦","🇸🇪","🇨🇭",
			"🇦🇪","🇻🇳"
		}
	};

	private SharedPreferences read;

	public emoji(Context ctx){
		c = ctx;
		read = ctx.getSharedPreferences("key",ctx.MODE_PRIVATE);
	}
	
	public View getLayout(View.OnClickListener ocl,View.OnClickListener goBack,View.OnClickListener delete){
		ml = new LinearLayout(c);
		ml.setLayoutParams(new LinearLayout.LayoutParams(-1,-1));
		ml.setOrientation(LinearLayout.VERTICAL);
		if(Build.VERSION.SDK_INT >= 14)
			ml.setMotionEventSplittingEnabled(false);
		ml.addView(getEmojiTabs(goBack,delete));
		ml.addView(getEmojiScrollLayout(ocl));
		return ml;
	}

	private View getEmojiTabs(View.OnClickListener goBack,View.OnClickListener delete){
		if(tabs == null){
			et = new LinearLayout(c);
			et.setBackgroundColor(Color.GRAY);
			LinearLayout hll = new LinearLayout(c);
			if(Build.VERSION.SDK_INT >= 14){
				et.setMotionEventSplittingEnabled(false);
			}
			et.setGravity(Gravity.CENTER);
			et.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(1),-2,1));
			tabs = new View[emojis.length];
			TextView tv = new TextView(c);
			tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12),getEmojiWidth(12),1));
			tv.setOnClickListener(goBack);
			tv.setTextColor(0xFF000000);
			tv.setTextSize(2*getEmojiTextSize()/5);
			tv.setGravity(Gravity.CENTER);
			tv.setText("abc");
			hll.addView(tv);
			HorizontalScrollView hsw = new HorizontalScrollView(c);
			hll.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(1),-2,1));
			hll.setGravity(Gravity.CENTER);
			for(int i = 0;i < emojis.length;i++){
				tv = new TextView(c);
				tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12),getEmojiWidth(12),1));
				tv.setOnClickListener(getEmojiTabOCL);
				tv.setTag(i);
				tv.setTextColor(0xFF000000);
				tv.setTextSize(4*getEmojiTextSize()/5);
				tv.setGravity(Gravity.CENTER);
				tv.setText(emojis[i][0]);
				if(i == 0) setSel(tv,true);
				hll.addView(tabs[i] = tv);
			}
			hsw.addView(hll);
			et.addView(hsw);
			tv = new TextView(c);
			tv.setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(12),getEmojiWidth(12),1));
			tv.setOnClickListener(delete);
			tv.setTextColor(0xFF000000);
			tv.setBackgroundResource(R.drawable.del);
			tv.setGravity(Gravity.CENTER);
			hll.addView(tv);
		}
		et.setGravity(Gravity.CENTER);
		return et;
	}

	private View.OnClickListener getEmojiTabOCL = 
	new View.OnClickListener(){
		public void onClick(View v){
			setSelection((int)v.getTag());
		}
	};

	public void setSelection(int index){
		if(index != selected){
			for(View x : tabs) setSel(x,false);
			setSel(tabs[index],true);
		}
	}

	private void setSel(View v,boolean b){
		v.setBackgroundColor(b ? 0x99FFFFFF : 0);
		if(b){
			if(ml.getChildCount() >= 2)
				ml.removeViewAt(1);
			selected = Integer.parseInt(v.getTag().toString());
			if(!start){
				ml.addView(getEmojiScrollLayout(o));
			} else start = false;
		}
	}

	private LinearLayout getEmojiScrollLayout(View.OnClickListener ocl){
		LinearLayout ll = new LinearLayout(c);
		LinearLayout llx = new LinearLayout(c);
		ScrollView sw = new ScrollView(c);
		ll.addView(sw);
		sw.addView(llx);
		if(Build.VERSION.SDK_INT >= 14)
			llx.setMotionEventSplittingEnabled(false);
		llx.setOrientation(LinearLayout.VERTICAL);
		LinearLayout[] satirlar = new LinearLayout[(emojis[selected].length/7)+1];
		sw.setLayoutParams(new LinearLayout.LayoutParams(-1,(40*Resources.getSystem().getDisplayMetrics().heightPixels)/100,1));
		TextView[] emojiler =new TextView[emojis[selected].length];
		for(int i=0;i!=(emojis[selected].length/7)+1;i++){
			satirlar[i] = new LinearLayout(c);
			if(Build.VERSION.SDK_INT >= 14)
				satirlar[i].setMotionEventSplittingEnabled(false);
			satirlar[i].setOrientation(LinearLayout.HORIZONTAL);
			satirlar[i].setLayoutParams(new LinearLayout.LayoutParams(-1,-1,1));
			llx.addView(satirlar[i]);
		}
		for(int i=0;i!=emojis[selected].length;i++){
			emojiler[i] = new TextView(c);
			emojiler[i].setText(emojis[selected][i]);
			emojiler[i].setGravity(Gravity.CENTER);
			emojiler[i].setOnClickListener(o = ocl);
			emojiler[i].setTextSize(getEmojiTextSize());
			emojiler[i].setTextColor(0xFF000000);
			emojiler[i].setSingleLine(true);
			emojiler[i].setLayoutParams(new LinearLayout.LayoutParams(getEmojiWidth(7),-2));
			satirlar[i/7].addView(emojiler[i]);
		}
		ll.setBackgroundColor(Color.WHITE);
		return ll;
	}

	public float getDensity(){
		return Resources.getSystem().getDisplayMetrics().density;
	}

	public float getEmojiTextSize(){
		DisplayMetrics dm = Resources.getSystem().getDisplayMetrics();
		return (dm.widthPixels / 7) / (getDensity() * 1.8f);
	}

	public int getEmojiWidth(int oran){
		return Resources.getSystem().getDisplayMetrics().widthPixels / oran;
	}

}

