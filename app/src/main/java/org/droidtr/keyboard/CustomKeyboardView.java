package org.droidtr.keyboard;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.inputmethodservice.*;
import android.os.*;
import android.util.*;
import android.inputmethodservice.Keyboard.*;
import android.view.inputmethod.*;

public class CustomKeyboardView extends KeyboardView
{
	SharedPreferences read;
	private CharSequence keylabel;
	private int butcolor;
	private int butshadow;
	private Drawable d;
	public int mainsize;
	public int upsize;
	private Paint paint;
	private int modifier;
	public CustomKeyboardView(Context ctx,AttributeSet as){
		super(ctx,as);
		read = ctx.getSharedPreferences("key",ctx.MODE_PRIVATE);
		if(read.getBoolean("activedraw",true)){
			paint = new Paint();
			butcolor = read.getInt("butcolor",Color.WHITE);
			butshadow = read.getInt("butshadow",Color.BLACK);
			modifier = read.getInt("textSizeModifier",10);
			paint.setShadowLayer(5,0,0,butshadow);
			paint.setAntiAlias(true);
			paint.setTextAlign(Paint.Align.CENTER);
			d=getBgDrawable();
		}
	}

	public void onDraw(Canvas c){
		if(null!=getKeyboard()){
		if(read.getBoolean("activedraw",true)){
			mainsize=(modifier*3*(getHeight()/50))/10;
			upsize=modifier*(getHeight()/25)/10;
			//iterasyon başı   
			for(Keyboard.Key key: getKeyboard().getKeys()) {
			try{
					if(!key.sticky){
						drawBackground(d,c,key);
						//simgeler
						if(key.icon != null){
							drawKeyBackground(key.icon,c,key);
						}
						//ana harfler
						paint.setFakeBoldText(true);
						paint.setColor(butcolor);
						paint.setTextSize(mainsize);
						if(isShifted()){
							keylabel=key.label.toString().toUpperCase();
						}else{
							keylabel=key.label;
						}
						c.drawText(keylabel.toString(), key.x+(key.width/2), key.y+(key.height/2)+(3*key.height/20) , paint);
						//popup yazıları
						if(key.label.length()<2){
							paint.setFakeBoldText(false);
							paint.setColor(Color.argb(200,Color.blue(butcolor),Color.red( butcolor),Color.green( butcolor)));
							paint.setTextSize(upsize);
							c.drawText(key.popupCharacters.toString().split("")[key.popupCharacters.toString().length()], key.x+key.width-(key.width/3), key.y+(key.height/3) , paint);
						}}
				}catch(Exception e){}	
			}
		}else{
			super.onDraw(c);
		}
		}
    }
	@Override
	protected boolean onLongPress(Keyboard.Key popupKey)
	{
		if(!popupKey.repeatable){
		return super.onLongPress(popupKey);
		}else{
			return false;
		}
	}
	private void drawKeyBackground(Drawable npd, Canvas canvas, Keyboard.Key key) {
        npd.setBounds(key.x+(key.width/2)-(key.height/4), key.y+(key.height/2)-(key.height/4),key.x+(key.width/2)+(key.height/4), key.y+(key.height/2)+(key.height/4));
        npd.draw(canvas);
	}
	private void drawBackground(Drawable npd, Canvas canvas, Keyboard.Key key) {
        npd.setBounds(key.x, key.y,key.x+key.width, key.y+key.height);
        npd.draw(canvas);
	}
	public static Drawable getBg(int[] color,float radius,int strokeColor,int strokeWidth){
		GradientDrawable gd = new GradientDrawable();
		if(Build.VERSION.SDK_INT >= 16){
			gd.setColors(color);
			gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
		} else{
			gd.setColor(color[0]);
		}
		gd.setCornerRadius(radius);
		gd.setStroke(strokeWidth,strokeColor);
		return gd;
	}
	public Drawable getBgDrawable(){
		int[] colors= {read.getInt("primaryColor",Color.TRANSPARENT),read.getInt("secondaryColor",Color.TRANSPARENT)};
		return getBg(colors,Float.parseFloat(read.getInt("radius",10)+"f"),read.getInt("strokeColor",Color.TRANSPARENT),read.getInt("strokeWidth",5));
	}
}

