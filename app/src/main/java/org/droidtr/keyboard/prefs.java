package org.droidtr.keyboard;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import android.widget.SeekBar.*;

public class prefs extends Activity {
	LinearLayout.LayoutParams defparam = new LinearLayout.LayoutParams(
		LinearLayout.LayoutParams.MATCH_PARENT,
		LinearLayout.LayoutParams.WRAP_CONTENT);
	String[] basliklar,altBasliklar;
	int[] simgeler;
	View.OnClickListener denetci;
	View[] keyboardLayouts = new View[7],keyboardSizes = new View[2];
	int num=6;
	String val="";
	SharedPreferences read;
	SharedPreferences.Editor edit;
	public void onCreate(Bundle b){
		Configuration config = new Configuration();
		this.getApplicationContext().getResources().updateConfiguration(config, null);
		super.onCreate(b);
		read = getSharedPreferences("key",MODE_PRIVATE);
	    edit = read.edit();
		edit.putBoolean("exitrequest",true);
		edit.commit();
		setTitle(getString(R.string.settings));
			basliklar = new String[]{
			getString(R.string.butchange),
			getString(R.string.setvibrate),
			getString(R.string.setimage),
			getString(R.string.setbackground),
			getString(R.string.preview),
			getString(R.string.keylayout),
			getString(R.string.fdes)+" "+getString(R.string.fordev),
			getString(R.string.qdes)+" "+getString(R.string.fordev),
			getString(R.string.fdes)+" "+getString(R.string.fend),
			getString(R.string.qdes)+" "+getString(R.string.fend),
			getString(R.string.aqdes)+" "+getString(R.string.fant),
			getString(R.string.ades)+" "+getString(R.string.fant),
			getString(R.string.endes),
			getString(R.string.otobuyuk),
			getString(R.string.setlang),
			getString(R.string.emojionoff),
			getString(R.string.performance),
			getString(R.string.otoparantez),
			getString(R.string.suggestion),
			getString(R.string.themesupport),
			getString(R.string.keysound),
			getString(R.string.tabview)
		};
		
		altBasliklar = new String[]{
			getString(R.string.abutchange),
			getString(R.string.asetvibrate),
			getString(R.string.asetimage),
			getString(R.string.asetbackground),
			getString(R.string.apreview),
			getString(R.string.skeylayout),
			getString(R.string.fades)+" "+getString(R.string.fordev),
			getString(R.string.qades)+" "+getString(R.string.fordev),
			getString(R.string.fades)+" "+getString(R.string.fend),
			getString(R.string.qades)+" "+getString(R.string.fend),
			getString(R.string.aqades)+" "+getString(R.string.fant),
			getString(R.string.aades)+" "+getString(R.string.fant),
			getString(R.string.enades),
			getString(R.string.aotobuyuk),
			getString(R.string.asetlang),
			getString(R.string.aemojionoff),
			getString(R.string.aperformance),
			getString(R.string.aotoparantez),
			getString(R.string.asuggestion),
			getString(R.string.athemesupport),
			getString(R.string.akeysound),
			getString(R.string.atabview)
		};
		int deficon=0;
		simgeler = new int[]{
			R.drawable.caps,
			R.drawable.vibrate,
			R.drawable.picture,
			R.drawable.color,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			R.drawable.otobuyuk,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon,
			deficon
		};
		onseticon();
		denetci = new View.OnClickListener(){
			public void onClick(View v){
				switch((int)v.getTag()){
					case 0:
						startActivity(new Intent(prefs.this,butchange.class));
						break;
					case 1:
						startActivity(new Intent(prefs.this,setvibrate.class));
						break;
					case 2:
						startActivity(new Intent(prefs.this,image.class));
						break;
					case 3:
						startActivity(new Intent(prefs.this,color.class));
						break;
					case 4:
						edit.putBoolean("preview",!read.getBoolean("preview",false));
						
						onupdate();
						break;
					case 5:
						menuOpened = !menuOpened;
						for(View x : keyboardLayouts)
							x.setVisibility(menuOpened ? View.VISIBLE : View.GONE);
						onupdate();
						break;
					case 6:
						num=5;
						setkeyboard("trf");
						onupdate();
						break;
					case 7:
						num=6;
						setkeyboard("trq");
						onupdate();
						break;
					case 8:
						num=7;
						setkeyboard("trf2");
						onupdate();
						break;
					case 9:
						num=8;
						setkeyboard("trq2");
						onupdate();
						break;
					case 10:
						num=9;
						setkeyboard("aqvoid");
						onupdate();
						break;
					case 11:
						num=10;
						setkeyboard("tra");
						onupdate();
						break;
					case 12:
						num=11;
						setkeyboard("trq3");
						onupdate();
						break;
					case 13:
						edit.putBoolean("otobuyuk",!read.getBoolean("otobuyuk",true));
						
						onupdate();
						break;
					case 14:
						Intent intent = new Intent();
						intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.LanguageSettings"));
						startActivity(intent);
						break;
					case 15:
						edit.putBoolean("emoji",!read.getBoolean("emoji",true));
						
						onupdate();
						break;
					case 16:
						edit.putBoolean("lowmemory",!read.getBoolean("lowmemory",false));
						
						onupdate();
						break;
					case 17:
						edit.putBoolean("otoparantez",!read.getBoolean("otoparantez",true));
						
						onupdate();
						break;
					case 18:
						edit.putBoolean("suggestion",!read.getBoolean("suggestion",false));
						
						onupdate();
						break;
					case 19:
						edit.putBoolean("activedraw",!read.getBoolean("activedraw",true));
						
						onupdate();
						break;
					case 20:
						edit.putBoolean("mute",!read.getBoolean("mute",true));
						
						onupdate();
						break;
					case 21:
						edit.putBoolean("tabview",!read.getBoolean("tabview",true));

						onupdate();
						break;
				}
			}
		};
		setContentView(ana());
	}
	
	int dip = 0;
	public void setkeyboard(String layout){
		//final SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();
		edit.putString("keyboard",layout);
		
	}
	public LinearLayout ana(){
		SeekBar sb = new SeekBar(this);
		SeekBar sbl = new SeekBar(this);
		LinearLayout sbll = new LinearLayout(this);
		sbll.setOrientation(LinearLayout.VERTICAL);
		LinearLayout sblll = new LinearLayout(this);
		sblll.setOrientation(sbll.getOrientation());
		sbll.setLayoutParams(defparam);
		sblll.setLayoutParams(defparam);
		sb.setLayoutParams(defparam);
		sbl.setLayoutParams(defparam);
		final TextView tw = new TextView(this);
		tw.setLayoutParams(new LinearLayout.LayoutParams(-2,-2,0));
		final TextView twl = new TextView(this);
		twl.setLayoutParams(tw.getLayoutParams());
		tw.setPadding((int)tw.getTextSize()/2,0,0,0);
		twl.setPadding(tw.getPaddingLeft(),0,0,0);
		/*final SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
	    final SharedPreferences.Editor edit = getSharedPreferences("key",MODE_PRIVATE).edit();*/
		sb.setMax(200);
		tw.setGravity(Gravity.CENTER);
		tw.setText(getString(R.string.keyport)+": "+read.getInt("modifier_portrait",100)+"%");
		sb.setProgress(read.getInt("modifier_portrait",100));
		sbl.setMax(200);
		twl.setGravity(tw.getGravity());
		twl.setText(getString(R.string.keyland)+": "+read.getInt("modifier_landscape",150)+"%");
		sbl.setProgress(read.getInt("modifier_landscape",150));
		
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

				@Override
				public void onProgressChanged(SeekBar p1, int p2, boolean p3)
				{
					tw.setText(getString(R.string.keyport)+": "+p2+"%");
					edit.putInt("modifier_portrait",p2);
					edit.commit();
				}

				@Override
				public void onStartTrackingTouch(SeekBar p1)
				{
					// TODO: Implement this method
				}

				@Override
				public void onStopTrackingTouch(SeekBar p1)
				{
					// TODO: Implement this method
				}
			});
			sbl.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

				@Override
				public void onProgressChanged(SeekBar p1, int p2, boolean p3)
				{
					twl.setText(getString(R.string.keyland)+": "+p2+"%");
					edit.putInt("modifier_landscape",p2);
					edit.commit();
				}

				@Override
				public void onStartTrackingTouch(SeekBar p1)
				{
					// TODO: Implement this method
				}

				@Override
				public void onStopTrackingTouch(SeekBar p1)
				{
					// TODO: Implement this method
				}
			});
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(defparam);
		ll.setOrientation(LinearLayout.VERTICAL);
		LinearLayout ll2 = new LinearLayout(this);
		ll.setLayoutParams(defparam);
		ll2.setOrientation(LinearLayout.VERTICAL);
		ScrollView sw = new ScrollView(this);
		sw.setLayoutParams(new ScrollView.LayoutParams(
								ScrollView.LayoutParams.MATCH_PARENT,
								ScrollView.LayoutParams.MATCH_PARENT));
		ll.addView(sw);
		ll2.addView(sbll);
		ll2.addView(sblll);
		sbll.addView(tw);
		sbll.addView(sb);
		sblll.addView(twl);
		sblll.addView(sbl);
		keyboardSizes[0] = sbll;
		keyboardSizes[1] = sblll;
		sw.addView(ll2);
		for(int i = 0;i != simgeler.length;i++){
			ll2.addView(oge(basliklar[i],altBasliklar[i],simgeler[i],i));
		}
		return ll;
	}
	
	public View bosluk(){
		View v = new View(this);
		v.setLayoutParams(new LinearLayout.LayoutParams(dip/2,dip/2));
		return v;
	}
	
	public LinearLayout oge(String baslik, String altBaslik, int simge, int id){
		LinearLayout ml = new LinearLayout(this);
		ml.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
		ml.setTag(id);
		LinearLayout ll = new LinearLayout(this);
		ll.setLayoutParams(defparam);
		ll.setOrientation(LinearLayout.VERTICAL);
		for(int i = 0;i != 2;i++){
			TextView tv = new TextView(this);
			tv.setLayoutParams(new LinearLayout.LayoutParams(-1,-1,0.5f));
			tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, i == 0 ? 18 : 14);
			dip = i == 0 ? (int) tv.getTextSize() : dip;
			tv.setText(i == 0 ? baslik : altBaslik);
			tv.setSingleLine();
			tv.setEllipsize(TextUtils.TruncateAt.END);
			ll.addView(tv);
		}
		ll.setPadding(dip/3,0,0,0);
		ImageView iv = new ImageView(this);
		iv.setLayoutParams(new LinearLayout.LayoutParams((int)(dip*2.5f), (int)(dip*2.5f)));
		iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
		iv.setPadding(dip/2,dip/2,dip/2,dip/2);
		iv.setImageResource(simge);
		iv.setId(id);
		ml.addView(iv);
		ml.addView(ll);
		ml.setOnClickListener(denetci);
		if(id >= 6 && id <= 12){
			keyboardLayouts[id - 6] = ml;
			ml.setVisibility(View.GONE);
			ml.setPadding(dip*2,dip/3,dip*2,dip/3);
		} else ml.setPadding(0,dip/3,0,dip/3);
		return ml;
	}
	public void onupdate() {
		edit.commit();
		onseticon();
		for(int i=0;i!=simgeler.length;i++){
			ImageView iw = (ImageView)findViewById(i);
			iw.setImageResource(simgeler[i]);
		}
		
	}
	
	boolean menuOpened = false;

	public void onseticon(){
		//final SharedPreferences read = getSharedPreferences("key",MODE_PRIVATE);
		val = read.getString("keyboard","trq2");
		simgeler[4]=read.getBoolean("preview",false)?R.drawable.enabled:R.drawable.disabled;
		simgeler[5]=menuOpened?R.drawable.arrow_up:R.drawable.arrow_down;
		simgeler[6]=val.equals("trf")?R.drawable.enabled:R.drawable.disabled;
		simgeler[7]=val.equals("trq")?R.drawable.enabled:R.drawable.disabled;
		simgeler[8]=val.equals("trf2")?R.drawable.enabled:R.drawable.disabled;
		simgeler[9]=val.equals("trq2")?R.drawable.enabled:R.drawable.disabled;
		simgeler[10]=val.equals("aqvoid")?R.drawable.enabled:R.drawable.disabled;
		simgeler[11]=val.equals("tra")?R.drawable.enabled:R.drawable.disabled;
		simgeler[12]=val.equals("trq3")?R.drawable.enabled:R.drawable.disabled;
		simgeler[13]=read.getBoolean("otobuyuk",true)?R.drawable.enabled:R.drawable.disabled;
		simgeler[15]=read.getBoolean("emoji",true)?R.drawable.enabled:R.drawable.disabled;
		simgeler[16]=!read.getBoolean("lowmemory",false)?R.drawable.enabled:R.drawable.disabled;
		simgeler[17]=read.getBoolean("otoparantez",true)?R.drawable.enabled:R.drawable.disabled;
		simgeler[18]=read.getBoolean("suggestion",false)?R.drawable.enabled:R.drawable.disabled;
		simgeler[19]=read.getBoolean("activedraw",true)?R.drawable.enabled:R.drawable.disabled;
		simgeler[20]=read.getBoolean("mute",false)?R.drawable.enabled:R.drawable.disabled;
		simgeler[21]=read.getBoolean("tabview",false)?R.drawable.enabled:R.drawable.disabled;
	}
}
