package org.droidtr.keyboard;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import java.io.*;
import android.graphics.drawable.*;

public class image extends Activity {
	
	private final int SELECT_PHOTO = 1;
	private ImageView imageView;
	
	  @Override  
	  protected void onCreate(Bundle savedInstanceState) {  
		super.onCreate(savedInstanceState);  
		  requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.imagepicker);    
		this.setTitle("");
		    imageView = (ImageView)findViewById(R.id.imageView);    
		Drawable d = Drawable.createFromPath(Uri.fromFile(new File(getFilesDir()+"/image.png")).getPath());
		imageView.setImageDrawable(d);
		  imageView.setOnLongClickListener(new OnLongClickListener(){

				  @Override
				  public boolean onLongClick(View p1)
				  {
					  (new File(getFilesDir()+"/image.png")).delete();
					 imageView.setImageBitmap(null);
					  return false;
				  }
			  });
		    Button pickImage = (Button) findViewById(R.id.btn_pick);    
		    pickImage.setOnClickListener(new OnClickListener() {    
			
			@Override
			public void onClick(View view) {        
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, SELECT_PHOTO);
			}
		});
	  }  
	
	  @Override  
	  protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {   
		    super.onActivityResult(requestCode, resultCode, imageReturnedIntent);     
		
		    switch(requestCode) {     
		case SELECT_PHOTO:
			if(resultCode == RESULT_OK){
				try {
					Uri imageUri = imageReturnedIntent.getData();
					this.setTitle(imageUri.getPath());
					InputStream imageStream = getContentResolver().openInputStream(imageUri);
					Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
					selectedImage=Bitmap.createScaledBitmap(selectedImage,getWindowManager().getDefaultDisplay().getWidth(),(getWindowManager().getDefaultDisplay().getHeight()*40)/100,false);
					imageView.setImageBitmap(selectedImage);
					saveImageToInternalStorage(selectedImage);
				}catch (FileNotFoundException e) {}
				catch(IOError e){}
				catch(IOException e){}
				finally{}
			}
		    }    
	  }  
	
	  @Override  
	  public boolean onCreateOptionsMenu(Menu menu) {  
		    return true;    
	  }     
	
	public boolean saveImageToInternalStorage(Bitmap image) {
		
		try {
			FileOutputStream fos = openFileOutput("image.png", Context.MODE_PRIVATE);
			image=Bitmap.createScaledBitmap(image,getWindowManager().getDefaultDisplay().getWidth(),(getWindowManager().getDefaultDisplay().getHeight()*40)/100,false);
			image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
			fos.close();
			return true;
		}
		catch(IOError e){}
		catch(IOException e){}
		finally{}
		return false;
	}  
}
